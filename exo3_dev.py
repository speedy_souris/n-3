#coding:utf-8
"""
EXERCICE PYTHON #3
> créer un programme simulant un combat qui respecte les contarintes suivantes :
    - Deux joueurs, auxquels on demandera de choisir un pseudo
    - Les deux combattants démarent avec 250 points de vie chacun
    - Le combat se déroule en 4 attaques (joueurs1, joueurs2, joueurs1 et enfin Joueur2)

    - Chaque attaque est une tentative (si elle réussit, le joueur infligera un nombre de dégâts entre 0 et 100 -
                                        si elle échoue, l'attaque est raté, et c'est au tour de l'autre joueur)
    - A la fin du combat (les 4 attaques), on déclare le gagnant (celui à qui il reste le plus de points de vie)


> Indications :
    - Le déroulement du combat doit être logique et annocé à l'utilisateur (affichez du texte, décrivez ce qu'il se passe)
    - Coder dans un premier temps uniquement avec des affichages/saisies, variables, opérations, conditions.
    - Pour les plus avancés, vous pourrez optimiser ce code ensuite en l'adaptant avec vos connaissances (boucles, fonction, classe, etc.)
"""

# ---------------------------------------------------------------------
#|                                                                     |
#|                        CLASSE DU PROGRAMME                          |
#|                                                                     |
# ---------------------------------------------------------------------

import random
import time

class player:
    """
        cette class permet de gerer les combats de chaque joueur
        avec les variables suivantes:
                user_name     = nom du jour
                energy        = nombre de points de vie (energie)
                random_attack = attaque réussi / échoué
                random_damage = points de dégat par attaque
                damage        = points de dégats total
        et aussi la methode attack qui permet ou non d'infleger des dégâts
        sur l'autre joueur
    """

    def __init__(self, name):
        """ Constructeur pour la classe player"""
        self.user_name = name
        self.energy = 250
        self.random_attack = False
        self.random_damage = 0
        self.damage = 0

    def state_attack(self):
        """ Methode pour l'attaque de joueur """
        self.random_attack = random.randint(0, 1)
        self.random_attack = bool(self.random_attack)
        return self.random_attack

    def damage_attack(self):
        """ Methode pour infliger des dégât sur une attaque"""
        self.random_damage = random.randint(0, 100)
        return self.random_damage

# ---------------------------------------------------------------------
#|                                                                     |
#|                        DEBUT DU PROGRAMME                           |
#|                                                                     |
# ---------------------------------------------------------------------

print("\n\t\tLE JEU COMMENCE !\n")

print("CHOISISSEZ LE NOM DE VOS 2 COMBATTANTS !\n")

                # --------------------------
                #|                          |
                #| CHOIX DU NOM DES JOUEURS |
                #|                          |
                # --------------------------

# CHOIX DU JOUEUR 1
noname = True
letters = ''

while noname:
    user_name1 = input('Nom du joueur 1 : ')
    for letter in user_name1:
        if ord(letter.lower()) in range(0, 123) and len(user_name1) < 9:
            letters += letter
        else:
            print("\nLe nom ne doit contenir que des chiffres ou lettre minuscule ou [ \ ] ^ _ `\n")
            break
        user_name1 = letters
        player1 = player(user_name1.upper())
        noname = False

print(f"\nLe joueur 1 est bien inscrit avec le pseudo {player1.user_name}")

#                         -------------------------

# CHOIX DU JOUEUR 2
noname = True
letters = ''

time.sleep(2)
while noname:
    user_name2 = input('\nNom du joueur 2 : ')
    for letter in user_name2:
        if ord(letter.lower()) in range(0, 123) and len(user_name2) < 9:
            letters += letter
        else:
            print("\nLe nom ne doit contenir que des chiffres ou lettre minuscule ou [ \ ] ^ _ `\n")
            break
        user_name2 = letters
        player2 = player(user_name2.upper())
        noname = False

print(f"\nLe joueur 2 est bien inscrit avec le pseudo {player2.user_name}")

                # ------------------------
                #|                        |
                #| INVENTAIRE DES JOUEURS |
                #|                        |
                # ------------------------

time.sleep(2)
print("\nINVENTAIRE DU JOUEUR 1\n\
------")
print(f"Le joueur 1 se nomme {player1.user_name}")
print(f"Il dispose d'une quantité d'énergie de {player1.energy} points")
print("Il peut disposer d'une puissance de 100 points d'attaque")
time.sleep(2)
print("\nINVENTAIRE DU JOUEUR 2\n\
------")
print(f"Le joueur 2 se nomme {player2.user_name}")
print(f"Il dispose d'une quantité d'énergie de {player2.energy} points")
print("Il peut disposer d'une puissance de 100 points d'attaque")
time.sleep(2)
print("\n\t\tLE COMBAT COMMENCE !")
print(" ------------------------------------- ")
print("| Chaque Joueur dispose de 2 attaques |")
print("| Le joueur 1 attaque le premier      |")
print(" ------------------------------------- ")

#-----------------------------------------------------------------------
nb_attack = 1

while nb_attack <= 4:
    time.sleep(3)
    # joueur 1
    print(f"\nATTAQUE De {player1.user_name} SUR {player2.user_name}")
    nb_attack += 1
    # si l'attaque réussit
    if player1.state_attack():
        attack_damage1 = player1.damage_attack()
        player2.energy -= attack_damage1
        player1.damage += attack_damage1
        print(f"\n{player2.user_name} perd {attack_damage1} point(s) de vie")
        print(f"Il lui reste {player2.energy} points de vie")
    # si l'attaque échoue
    else:
        print(f"\n{player2.user_name} riposte et contre l'attaque")
        print(f"{player2.user_name} ne perd aucun point(s) de vie")
        print(f"Il lui reste {player2.energy} points de vie")

    time.sleep(3)
    # joueur 2
    print(f"\nATTAQUE De {player2.user_name} SUR {player1.user_name}")
    nb_attack += 1
    # si l'attaque réussit
    if player2.state_attack():
        attack_damage2 = player2.damage_attack()
        player1.energy -= attack_damage2
        player2.damage += attack_damage2
        print(f"\n{player1.user_name} perd {attack_damage2} point(s) de vie")
        print(f"Il lui reste {player1.energy} points de vie")
    # si l'attaque échoue
    else:
        print(f"\n{player1.user_name} riposte et contre l'attaque")
        print(f"{player1.user_name} ne perd aucun point(s) de vie")
        print(f"Il lui reste {player1.energy} points de vie")


#-----------------------------------------------------------------------
time.sleep(3)
# Résultat final
print("\nFIN DU COMBAT !\n")

# si joueur 1 gagnant
if player1.energy > player2.energy:
    print(f"le vainqueur de ce combat est {player1.user_name} avec {player1.energy} points de vies et a subit {player2.damage} points de dégâts")
    print(f"{player2.user_name} est le perdant avec {player2.energy} points de vies et a subit {player1.damage} points de dégâts\n")

# si joueur 2 gagnant
elif player1.energy < player2.energy:
    print(f"le vainqueur de ce combat est {player2.user_name} avec {player2.energy} points de vies et {player1.damage} points de dégâts")
    print(f"{player1.user_name} est le perdant avec {player1.energy} points de vies et {player2.damage} points de dégâts\n")

# si aucun gagnant
else:
    print(f"il ny a aucun gagnant les 2 joueurs ont le même nombre de point de vies")
    print(f"{player1.user_name} dispose de {player1.energy} point de vies et a subit {player2.damage} points d'attaque")
    print(f"{player2.user_name} dispose de {player2.energy} point de vies et a subit {player1.damage} points d'attaque\n")
