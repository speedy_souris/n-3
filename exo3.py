#coding:utf-8
"""
EXERCICE PYTHON #3
> créer un programme simulant un combat qui respecte les contarintes suivantes :
    - Deux joueurs, auxquels on demandera de choisir un pseudo
    - Les deux combattants démarent avec 250 points de vie chacun
    - Le combat se déroule en 4 attaques (joueurs1, joueurs2, et enfin Joueur2)

    - Chaque attaque est une tentative (si elle réussit, le joueur infligera un nombre de dégâts entre 0 et 100 -
                                        si elle échoue, l'attaque est raté, et c'est au tour de l'autre joueur)
    - A la fin du combat (les 4 4 attaques), on déclare le gagnant (celui à qui il reste le plus de points de vie)


> Indications :
    - Le déroulement du combat doit être logique et annocé à l'utilisateur (affichez du texte, décrivez ce qu'il se passe)
    - Coder dans un premier temps uniquement avec des affichages/saisies, variables, opérations, conditions.
    - Pour les plus avancés, vous pourrez optimiser ce code ensuite en l'adaptant avec vos connaissances (boucles, fonction, classe, etc.)
"""

import random

random_attack = True
random_damage = 0

print("\nLE COMBAT COMMENCE !\n")

#-----------------------------------------------------------------------
#1ére attaque
random_attack = random.randint(0, 1)
random_attack = bool(random_attack)

if random_attack == True:
    # si l'attaque réussit
    random_damage = randint(0, 100)

else:
    # si l'attaque échoue
    pass

#-----------------------------------------------------------------------
#2éme attaque
random_attack = random.randint(0, 1)
random_attack = bool(random_attack)

if random_attack == True:
    # si l'attaque réussit
    random_damage = randint(0, 100)

else:
    # si l'attaque échoue
    pass

#-----------------------------------------------------------------------
#3éme attaque
random_attack = random.randint(0, 1)
random_attack = bool(random_attack)

if random_attack == True:
    # si l'attaque réussit
    random_damage = randint(0, 100)

else:
    # si l'attaque échoue
    pass

#-----------------------------------------------------------------------
#4éme attaque
random_attack = random.randint(0, 1)
random_attack = bool(random_attack)

if random_attack == True:
    # si l'attaque réussit
    random_damage = randint(0, 100)

else:
    # si l'attaque échoue
    pass

#-----------------------------------------------------------------------
# Résultat final
print("\nFIN DU COMBAT !\n")
